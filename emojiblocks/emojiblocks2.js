(function () {
    let startDrawing = false;
    let brushsize = 1;
    let colorpicker = "basic";
    let currentColor = "ff0000";
    const textCopy = document.getElementById('texter');
    const colorLauncher = document.getElementById('color-launch');
    const clearBoard = document.getElementById('clear-board');
    const dimX = document.getElementById('dimensionsX');
    const dimY = document.getElementById('dimensionsY');
    const paintArea = document.getElementById('paintArea');
    const brushSizes = document.getElementsByName('brushes');
    const copier = document.getElementById('copytext');
    const colorHouse = document.getElementById("color-house");
    const undoButton = document.getElementById("undo");
    const redoButton = document.getElementById("redo");
    let startX = 25;
    let startY = 25;
    let backHistory = [];
    let nextHistory = [];
    let app = {
        cells: []
    };

    let colorPicker = {
        r: 0,
        g: 0,
        b: 0
    };

    const basicColors = [
        "ffffff00", "jason-statham", "000000", "1e1e1e", "3c3c3c",
        "5a5a5a", "787878", "969696", "b4b4b4", "ffffff",
        "ff0000", "ff3c3c", "ff5a5a", "ff9696", "ffd2d2",
        "ff7800", "ff963c", "ffb45a", "ffd296", "fff0d2",
        "f0ff00", "f0ff3c", "f0ff5a", "f0ff96", "ffffd2",
        "00ff00", "3cff3c", "5aff5a", "96ff96", "d2ffd2",
        "00ffff", "3cffff", "5affff", "96ffff", "d2ffff",
        "1e00ff", "3c3cff", "5a5aff", "9696ff", "d2d2ff",
        "7800ff", "963cff", "b45aff", "d296ff", "f0d2ff",
        "ff00ff", "ff3cff", "ff5aff", "ff96ff", "ffd2ff"
    ];

    function renderBoard() {
        let ims = "";
        if (app.cells.length < 1) {
            clearAll();
        }
        for (let r = 0; r < app.cells.length; r++) {
            if (r > 0) {
                ims += "<br>";
            }
            for (let c = 0; c < app.cells[r].length; c++) {
                let suffix = app.cells[r][c] == "jason-statham" ? ".gif" : ".png";

                ims += `<img src="images/${app.cells[r][c]}${suffix}" id="r${r}c${c}" data-col="${c}" data-row="${r}">`
            }
        }
        ims += `<div id="cursorfollower"></div>`;
        paintArea.innerHTML = ims;
        paintArea.style.width = (app.cells[0].length * 20) + "px";
        imageToSlack();
        syncDimensions();
        syncColor();
        localStorage.app = JSON.stringify(app);
    }

    function clearAll() {
        app.cells = [];
        for (let r = 0; r < startY; r++) {
            app.cells.push([]);
            for (let c = 0; c < startX; c++) {
                app.cells[r].push("spaces");
            }
        }
    }

    function syncColor() {
        let suffix = currentColor == "jason-statham" ? ".gif" : ".png";
        colorLauncher.style.backgroundImage = `url(images/${currentColor}${suffix})`;
        colorLauncher.innerHTML = `<img src="images/${currentColor}${suffix}">`;
    }

    function syncDimensions() {
        dimX.value = startX = app.cells[0].length;
        dimY.value = startY = app.cells.length;
    }

    function getGlobalColor() {
        return "rgba(" + colorPicker.r + "," + colorPicker.g + "," + colorPicker.b + ")";
    }

    function getContrastColor() {
        var opposing = "rgba(";
        for (var colorKey in colorPicker) {
            var val = colorPicker[colorKey];
            if (val <= 64) {
                opposing += 255;
            } else if (val <= 128) {
                opposing += 192;
            } else if (val <= 192) {
                opposing += 64;
            } else {
                opposing += 0;
            }
            opposing += (colorKey !== "a" ? "," : "");
        }
        return opposing;
    }

    undoButton.onclick = function () {
        if (undoButton.className == "") {
            var replacer = backHistory.pop();
            nextHistory.unshift(replacer);
            redoButton.className = "";
            app = JSON.parse(replacer);
            if (backHistory.length < 1) {
                undoButton.className = "disabled";
            }
            renderBoard();
        }
    };
    redoButton.onclick = function (e) {
        if (redoButton.className == "") {
            var replacer = nextHistory.shift();
            backHistory.push(replacer);
            undoButton.className = "";
            app = JSON.parse(replacer);
            if (nextHistory.length < 1) {
                redoButton.className = "disabled";
            }
            renderBoard();
        }
    };

    paintArea.ontouchstart = function (e) {
        e.preventDefault();
        startDrag(e);
        return false;
    };
    paintArea.ontouchmove = function (e) {
        moveDrag(e);
    };
    paintArea.ontouchend = function (e) {
        e.preventDefault();
        stopDrag(e);
        return false;
    };
    paintArea.onmousedown = function (e) {
        e.preventDefault();
        startDrag(e);
        moveDrag(e);
    };
    paintArea.onmousemove = function (e) {
        moveDrag(e);
    };
    paintArea.onmouseup = function (e) {
        stopDrag(e);
    };

    function offsetFind(e, pos) {
        let offs = 0;
        let origPos = pos == 'top' ? e.pageY : e.pageX;
        let el = paintArea;
        while (el.offsetParent !== null) {
            offs += pos == 'top' ? el.offsetTop : el.offsetLeft;
            el = el.offsetParent;
        }
        return (origPos - offs - ((brushsize * 20) / 2)) + 'px';
    }

    function startDrag(e) {
        startDrawing = true;
    }

    function moveDrag(e) {
        var cursBlock = document.getElementById('cursorfollower');
        cursBlock.style.width = cursBlock.style.height = (brushsize * 20) + 'px';
        cursBlock.style.left = (offsetFind(e, 'left'));
        cursBlock.style.top = (offsetFind(e, 'top'));
        let suffix = currentColor == "jason-statham" ? ".gif" : ".png";

        var targ = e.target;
        if (targ && targ.tagName == "IMG" && startDrawing) {
            e.target.src = 'images/' + currentColor + suffix;
            let row = Number(targ.getAttribute('data-row'));
            let col = Number(targ.getAttribute('data-col'));
            app.cells[row][col] = currentColor;

            if (brushsize == 3) {
                let allCells = [
                    `r${row - 1}c${col - 1}`,
                    `r${row - 1}c${col}`,
                    `r${row - 1}c${col + 1}`,
                    `r${row}c${col - 1}`,
                    `r${row}c${col + 1}`,
                    `r${row + 1}c${col - 1}`,
                    `r${row + 1}c${col}`,
                    `r${row + 1}c${col + 1}`
                ];
                allCells.forEach(cell => {
                    let cellEl = document.getElementById(cell);
                    if (cellEl) {
                        cellEl.src = 'images/' + currentColor + suffix;
                        let row = Number(cellEl.getAttribute('data-row'));
                        let col = Number(cellEl.getAttribute('data-col'));
                        app.cells[row][col] = currentColor;
                    }
                })
            }
        }
    }

    function stopDrag(e, outsideDrawingArea) {
        startDrawing = false;
        if (!outsideDrawingArea) {
            saveToApp();
        }
        imageToSlack();
    }
    colorHouse.addEventListener('click', (e) => {
        if (e.target == colorHouse) {
            colorHouse.className = "";
        }
    });

    copier.addEventListener('click', (e) => {
        textCopy.select();
        document.execCommand("copy");
        copier.innerText = "Copied";
        setTimeout(() => {
            copier.innerText = "Copy Slackmoji";
        }, 1000);
    });

    for (var b = 0; b < brushSizes.length; b++) {
        brushSizes[b].addEventListener('change', (e) => {
            if (e.target.checked) {
                brushsize = Number(e.target.value);
            }
        });
    }

    document.addEventListener('click', (e) => {
        if (e.target.id !== "paintArea" && e.target.getAttribute('data-row') == null) {
            stopDrag(e, true);
        }
    })
    colorLauncher.addEventListener('click', (e) => {
        showColorPicker()
    });
    clearBoard.addEventListener('click', (e) => {
        var deletion = confirm("Clear board and start anew?");
        if (deletion) {
            clearAll();
            renderBoard();
        }
    });

    dimX.addEventListener('change', (e) => {
        changeDimensions(e, 'x')
    });
    dimY.addEventListener('change', (e) => {
        changeDimensions(e, 'y')
    });

    function changeDimensions(e, dim) {
        var newVal = Number(e.target.value);
        if (newVal > 9 && newVal < 41) {

            if (dim == 'x') {
                var oldVal = app.cells[0].length;
                var diff = newVal - oldVal;
                if (newVal < oldVal) {
                    for (var r = 0; r < app.cells.length; r++) {
                        app.cells[r].splice(diff);
                    }
                } else if (oldVal < newVal) {
                    let newArray = [];
                    for (var n = 0; n < Math.abs(diff); n++) {
                        newArray.push('spaces');
                    }
                    for (var r = 0; r < app.cells.length; r++) {
                        app.cells[r] = app.cells[r].concat(newArray);
                    }
                }
            } else {
                var oldVal = app.cells.length;
                var diff = newVal - oldVal;
                if (newVal < oldVal) {
                    app.cells.splice(diff);
                } else if (oldVal < newVal) {

                    for (var g = 0; g < Math.abs(diff); g++) {
                        var newArray = [];
                        for (var n = 0; n < app.cells[0].length; n++) {
                            newArray.push('spaces');
                        }
                        app.cells.push(newArray);
                    }
                }
            }
            renderBoard();
        }
        syncDimensions();
    }

    function Element(type, options) {
        var element = document.createElement(type);
        for (var option in options) {
            if (option.indexOf("data-") === 0) {
                element.setAttribute(option, options[option]);
            } else if (option === "style") {
                for (var style in options.style) {
                    element.style[style] = options.style[style];
                }
            } else {
                element[option] = options[option];
            }
        }
        return element;
    }

    function showColorPicker(startingColor) {
        var tempColor = startingColor ? startingColor : currentColor;
        colorPicker = hexToRgb(tempColor);
        colorHouse.innerHTML = "";
        colorHouse.className = "showing";
        var colorBacker = new Element("div", {
            className: "color-backer"
        });
        var colorIndex = 0;

        var basicColorButton = new Element("button", {
            className: "colortype basic " + (colorpicker === "basic" ? "" : "behind"),
            innerHTML: "Basic",
            title: "Basic",
            disabled: colorpicker === "basic",
            onclick: function () {
                colorpicker = "basic";
                showColorPicker(tempColor);
            }
        });

        var customColorButton = new Element("button", {
            className: "colortype custom " + (colorpicker === "custom" ? "" : "behind"),
            innerHTML: "Custom",
            title: "Custom",
            disabled: colorpicker === "custom",
            onclick: function () {
                colorpicker = "custom";
                showColorPicker(tempColor);
            }
        });

        colorBacker.appendChild(basicColorButton);
        colorBacker.appendChild(customColorButton);

        if (colorpicker === "basic") {
            for (var bc = 0; bc < basicColors.length; bc++) {
                var basicColor = new Element("button", {
                    className: "color-picker " + (basicColors[bc] == "ffffff00" ? "checkers " : "") + (tempColor == basicColors[bc] ? "active" : ""),
                    innerHTML: "<span style=\"background-color: #" + basicColors[bc] + ";\"></span>",
                    value: basicColors[bc],
                    onclick: function (e) {
                        colorHouse.className = "";
                        currentColor = e.target.value;
                        syncColor();
                    }
                });
                colorBacker.appendChild(basicColor);
            }
        } else if (colorpicker === "custom") {
            for (var color in colorPicker) {
                var slider = new Element("div", {
                    className: "color-slider checkers"
                });
                var sliderInner = new Element("div", {
                    className: "slider-inner"
                });
                var colorVal = colorPicker[color];
                var leftColor = "rgb(";
                var rightColor = "rgb(";
                var cssIndex = 0;
                for (var cols in colorPicker) {
                    if (cssIndex == colorIndex) {
                        leftColor += 0;
                        rightColor += 255;

                    } else {
                        leftColor += colorPicker[cols];
                        rightColor += colorPicker[cols];
                    }
                    leftColor += (cols === "b" ? "" : ",");
                    rightColor += (cols === "b" ? "" : ",");
                    cssIndex++;
                }
                leftColor += ")";
                rightColor += ")";
                var colorIndicator = (colorVal / 270) * 100;
                sliderInner.style.backgroundImage = `linear-gradient(-54deg, #cccccc 40%, gray 40%, transparent 50%), linear-gradient(54deg, #cccccc 40%, gray 40%, transparent 50%), linear-gradient(to right, ${leftColor}, ${rightColor})`;
                sliderInner.style.backgroundPosition = `calc(${colorIndicator}% - 5px) 100%, calc(${colorIndicator}% + 5px) 100%, 0 0`
                var sliderInput = new Element("input", {
                    className: color,
                    type: "range",
                    min: 0,
                    max: 270,
                    step: 30,
                    value: colorVal,
                    onchange: function (e) {
                        var color = e.target.className;
                        colorPicker[color] = e.target.value;
                        tempColor = fullRgbToHex();
                        showColorPicker(tempColor);
                    }
                });
                sliderInner.appendChild(sliderInput);
                slider.appendChild(sliderInner);
                colorBacker.appendChild(slider);
                colorIndex++;
            }

            var showingValues = new Element("div", {
                className: "bottom-section checkers"
            });

            var colorResult = new Element("div", {
                className: "result"
            });
            colorResult.style.background = getGlobalColor();
            colorResult.style.borderColor = getContrastColor();
            showingValues.appendChild(colorResult);
            colorBacker.appendChild(showingValues);

            var cancelButton = new Element("button", {
                className: "cancel bot",
                innerHTML: "Cancel",
                title: "Cancel",
                onclick: function () {
                    colorHouse.className = "";
                }
            });

            var saveButton = new Element("button", {
                className: "save bot",
                innerHTML: "Save",
                title: "Save",
                onclick: function (e) {
                    colorHouse.className = "";
                    currentColor = tempColor;
                    syncColor();
                }
            });
            colorBacker.appendChild(cancelButton);
            colorBacker.appendChild(saveButton);
        }

        colorHouse.appendChild(colorBacker);
    }

    function saveToApp() {
        let currentApp = JSON.stringify(app);
        backHistory.push(currentApp);
        if (backHistory.length > 10) {
            backHistory.shift();
        }
        undoButton.className = "";
        localStorage.app = currentApp;
    }

    function hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : {
            r: 0,
            g: 0,
            b: 0
        };
    }

    function rgbToHex(rgb) {
        var rgbVal = rgb > 255 ? 255 : rgb;
        var hex = Number(rgbVal).toString(16);
        return (hex.length < 2 ? "0" + hex : hex);
    };

    function fullRgbToHex() {
        var colString = [];
        for (var p in colorPicker) {
            colString += rgbToHex(roundedRGB(colorPicker[p]))
        }
        return colString;
    };

    function roundedRGB(num) {
        return num == 255 ? num : (Math.round(num / 30) * 30);
    }

    function imageToSlack() {
        var slack = '';
        for (var r = 0; r < app.cells.length; r++) {
            if (r > 0) {
                slack += '\n';
            }
            for (var c = 0; c < app.cells[r].length; c++) {
                var hex = app.cells[r][c] == 'ffffff00' ? 'spaces' : app.cells[r][c];
                var suffix = hex == "001e00" || hex == "1e0000" ? "h" : "";
                slack += `:${hex}${suffix}:`;
            }
        }
        textCopy.value = slack;
    }

    if (localStorage.app) {
        let parsedApp = JSON.parse(localStorage.app);
        if (parsedApp.cells && parsedApp.cells.length) {
            app = parsedApp;
            prevApp = parsedApp;
        }
    }
    renderBoard();

})();