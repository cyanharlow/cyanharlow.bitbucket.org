(function () {
    if (!document.getElementById("rainbows")) {
        const styleSheet = document.createElement('link');
        styleSheet.id = "rainbows";
        styleSheet.href = "https://cyanharlow.bitbucket.io/rainbows.css";
        styleSheet.rel = "stylesheet";
        document.body.appendChild(styleSheet);
    }
    const percentage = Number(document.getElementsByClassName("bar")[1].style.width.replace("%", ""));
    document.getElementsByClassName('progress')[0].innerHTML = Math.round(percentage) + "%";

    const av = document.getElementsByClassName("aui-avatar-inner")[0].children[0];
    if (av.getAttribute("alt") === "Diana Smith") {
        av.setAttribute("src", "https://cyanharlow.bitbucket.io/avatar.jpg")
    }

})();