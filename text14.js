(function () {

    const defaultText = `Jived fox
nymph grabs
quick waltz`;

    const fonts = {
        "fpf": {
            label: "Pride",
            styles: [{id: "r", name: "rainbow"}, {id: "l", name: "lesbian"}, {id: "g", name: "gay"}, {id: "b", name: "bisexual"}, {id: "n", name: "nonbinary"},{id: "a", name: "asexual"},  {id: "p", name: "pansexual"}, {id: "t", name: "trans"}],
            allcaps: true,
            displayedStyle: "r",
            categories: ["decor", "sans"]
        },
        "lff": {
            label: "Long Frog",
            allcaps: true,
            capped: true,
            customSpace: true,
            categories: ["decor", "sans"]
        },
        "eggs": {
            label: "Eggs",
            allcaps: true,
            categories: ["decor", "sans"]
        },
        "alphabongo": {
            label: "Alphabongo",
            allcaps: true,
            animated: true,
            categories: ["sans", "animated"]
        },
        "ywd": {
            label: "You wouldn't",
            allcaps: true,
            customSpace: true,
            categories: ["sans", "decor"]
        },
        "ddg": {
            label: "DDG",
            animated: true,
            allcaps: true,
            categories: ["animated", "sans"]
        },
        "statham": {
            label: "Statham",
            animated: true,
            allcaps: true,
            categories: ["animated", "sans"]
        },
        "diamond": {
            label: "Diamond",
            animated: true,
            allcaps: true,
            categories: ["animated", "sans", "decor"]
        },
        "money": {
            label: "Money",
            animated: true,
            allcaps: true,
            categories: ["animated", "serif", "decor"]
        },
        "alphablobby": {
            label: "Blobby",
            animated: true,
            allcaps: true,
            categories: ["animated", "sans"]
        },
        "loz": {
            label: "Lozenge",
            styles: [{id: "", name: "plain"}, {id: "1", name: "Red"}, {id: "2", name: "Green"}],
            customSpace: true,
            capped: true,
            allcaps: true,
            categories: ["decor", "sans"]
        },
        "frnds": {
            label: "Friends",
            inserter: 3,
            allcaps: true,
            categories: ["decor"]
        },
        "honks": {
            label: "Honk",
            customSpace: true,
            categories: ["decor"]
        },
        "poop": {
            label: "Poop",
            categories: ["decor"]
        },
        "bounce": {
            label: "Bounce",
            animated: true,
            categories: ["animated", "sans"]
        },
        "stsf": {
            label: "Serif",
            styles: [{name: "Rainbow", id: "rainbow"}, {id: "0", name: "red"}, {id: "1", name: "orange"}, {id: "2", name: "yellow"}, {id: "3", name: "green"}, {id: "4", name: "aqua"}, {id: "5", name: "teal"}, {id: "6", name: "blue"}, {id: "7", name: "purple"}, {id: "8", name: "pink"}],
            displayedStyle: "rainbow",
            categories: ["serif"]
        },
        "supreme": {
            label: "Supreme",
            customSpace: true,
            categories: ["sans"]
        },
        "calc": {
            label: "Calculator",
            categories: ["decor"]
        },
        "glitter": {
            label: "Glitter",
            animated: true,
            categories: ["animated", "sans"]
        },
        "8ies": {
            label: "Eighties",
            categories: ["decor", "sans"]
        },
        "comicsans": {
            label: "Comic Sans",
            categories: ["sans"]
        },
        "fest": {
            label: "Festive",
            categories: ["decor", "sans"]
        },
        "bones": {
            label: "Bones",
            categories: ["decor"]
        },
        "caution": {
            label: "Caution Tape",
            customSpace: true,
            categories: ["decor", "sans"]
        },
        "eh": {
            label: "Enhance",
            styles: [{id: "x", name: "1x"}, {id: "2x", name: "2x"}],
            displayedStyle: "x",
            categories: ["decor", "sans"]
        },
        "uwu": {
            label: "uwu",
            categories: ["decor", "sans"]
        },
        "fancy": {
            label: "Fancy",
            categories: ["decor", "serif"]
        },
        "fire": {
            label: "Fire",
            animated: true,
            customSpace: true,
            categories: ["animated", "sans"]
        },
        "gaudy": {
            label: "Gaudy",
            categories: ["decor", "serif"]
        },
        "horror": {
            label: "Horror",
            categories: ["decor"]
        },
        "jelly": {
            label: "Jelly",
            animated: true,
            categories: ["animated", "sans"]
        },
        "kanjq": {
            label: "Kanji-esque",
            categories: ["decor"]
        },
        "metal": {
            label: "Metal",
            categories: ["decor", "sans"]
        },
        "neon": {
            label: "Neon",
            categories: ["decor", "sans"]
        },
        "retro": {
            label: "Retro",
            categories: ["serif"]
        },
        "rusprop": {
            label: "Propaganda",
            categories: ["decor"]
        },
        "snow": {
            label: "Snow",
            animated: true,
            categories: ["animated", "sans"]
        },
        "business": {
            label: "Business",
            categories: ["serif"]
        },
        "ztoa": {
            label: "Sans",
            styles: [{name: "rainbow", id: "rainbow"}, {id: "0", name: "red"}, {id: "1", name: "orange"}, {id: "2", name: "yellow"}, {id: "3", name: "green"}, {id: "4", name: "aqua"}, {id: "5", name: "teal"}, {id: "6", name: "blue"}, {id: "7", name: "purple"}, {id: "8", name: "pink"}],
            displayedStyle: 5,
            categories: ["sans"]
        }
    };

    const keyChart = {
        "_a": "lower-a",
        "_b": "lower-b",
        "_c": "lower-c",
        "_d": "lower-d",
        "_e": "lower-e",
        "_f": "lower-f",
        "_g": "lower-g",
        "_h": "lower-h",
        "_i": "lower-i",
        "_j": "lower-j",
        "_k": "lower-k",
        "_l": "lower-l",
        "_m": "lower-m",
        "_n": "lower-n",
        "_o": "lower-o",
        "_p": "lower-p",
        "_q": "lower-q",
        "_r": "lower-r",
        "_s": "lower-s",
        "_t": "lower-t",
        "_u": "lower-u",
        "_v": "lower-v",
        "_w": "lower-w",
        "_x": "lower-x",
        "_y": "lower-y",
        "_z": "lower-z",
        "_0": "0",
        "_1": "1",
        "_2": "2",
        "_3": "3",
        "_4": "4",
        "_5": "5",
        "_6": "6",
        "_7": "7",
        "_8": "8",
        "_9": "9",
        "_A": "a",
        "_B": "b",
        "_C": "c",
        "_D": "d",
        "_E": "e",
        "_F": "f",
        "_G": "g",
        "_H": "h",
        "_I": "i",
        "_J": "j",
        "_K": "k",
        "_L": "l",
        "_M": "m",
        "_N": "n",
        "_O": "o",
        "_P": "p",
        "_Q": "q",
        "_R": "r",
        "_S": "s",
        "_T": "t",
        "_U": "u",
        "_V": "v",
        "_W": "w",
        "_X": "x",
        "_Y": "y",
        "_Z": "z",
        "_Z": "z",
        "_!": "_exclamation",
        "_?": "_question",
        "_!": "_exclamation",
        "_%": "_percent",
        "_&": "_ampersand",
        "_*": "_asterisk",
        "_@": "_at",
        "_^": "_carat",
        "_:": "_colon",
        "_.": "_period",
        "_;": "_semicolon",
        "_,": "_comma",
        "_+": "_plus",
        "_-": "_minus",
        "_=": "_equal",
        "_>": "_greaterthan",
        "_<": "_lessthan",
        "_#": "_hash",
        "_[": "_leftbracket",
        "_]": "_rightbracket",
        "_{": "_leftcurly",
        "_}": "_rightcurly",
        "_“": "_leftdquote",
        "_”": "_rightdquote",
        "_‘": "_leftsquote",
        "_’": "_rightsquote",
        "_`": "_backtick",
        "_\\": "_backslash",
        "_/": "_forwardslash",
        "_'": "_rightsquote",
        "_\"": "_rightdquote",
        "_(": "_leftparen",
        "_)": "_rightparen",
        "__": "_underscore",
        "_~": "_tilde",
        "_|": "_vertical",
        "_$": "_dollar"
    };
    const textinput = document.getElementById("textinput");
    const searchinput = document.getElementById("searchinput");
    const clearsearch = document.getElementById("clearsearch");
    const categories = document.getElementsByName("category");
    const examples = document.getElementById("examples");

    function conversion() {
        const rainbowMaxColors = 9;
        let category;
        for (let i = 0; i < categories.length; i++) {
            if (categories[i].checked) {
                category = categories[i].value;
                break;
            }
        }
        let fontsFound = 0;
        examples.innerHTML = "";

        for (let font in fonts) {

            let searchVal = searchinput.value.toLowerCase();
            let fontSearchString = (font + " " + fonts[font].label).toLowerCase();
            let isInSearchField = fontSearchString.indexOf(searchVal) > -1 || searchVal == "";
            let isInFontCategories = fonts[font].categories.indexOf(category) > -1 || category == "all";

            if (isInSearchField && isInFontCategories) {
                fontsFound++;
                let typeFace = font;
                let rainbowSwitch = 0;
                let replaced = "";
                let displayed = "";

                let imageSuffix = fonts[font].animated ? "gif" : "png";

                if (fonts[font].displayedStyle) {
                    typeFace = font + fonts[font].displayedStyle;
                }

                let space = fonts[font].customSpace ? `${typeFace}-_space` : `space`;
                let texts = textinput.value.length ? textinput.value : fonts[font].label;

                if (fonts[font].allcaps) {
                    texts = texts.toUpperCase();
                }

                let inserterInt = 0;
                for (let i = 0; i < texts.length; i++) {

                    let prevChar = i == 0 ? ' ' : texts.charAt(i - 1);
                    let inserterHTML = '';
                    let replacerText = '';
                    if (fonts[font].displayedStyle == "rainbow") {
                        typeFace = font + rainbowSwitch;
                    }
                    let char = texts.charAt(i);
                    let convertedChar = keyChart["_" + char];

                    if (fonts[font].inserter && prevChar !== ' ' && prevChar !== "\n" && prevChar !== "\r") {
                        inserterInt = inserterInt < fonts[font].inserter ? inserterInt + 1 : 1;
                        inserterHTML = `<img src="letters/${typeFace}/${typeFace}-ins${inserterInt}.${imageSuffix}">`;
                        replacerText = `:${typeFace}-ins${inserterInt}:`;
                    }
                    if (i == 0 && fonts[font].capped) {
                        replaced += `:${typeFace}-cap-pre:`;
                        displayed += `<img src="letters/${typeFace}/${typeFace}-cap-pre.${imageSuffix}">`;
                    }
                    if (convertedChar) {
                        replaced += replacerText;
                        replaced += `:${typeFace}-${convertedChar}:`;
                        if (i < 48) {
                            displayed += inserterHTML;
                            displayed += `<img src="letters/${typeFace}/${typeFace}-${convertedChar}.${imageSuffix}">`;
                        }
                    } else if (char == " ") {
                        rainbowSwitch--;
                        replaced += `:${space}:`;
                        if (i < 48) {
                            displayed += `<img src="letters/${fonts[font].customSpace ? `${typeFace}/`: ``}${space}.png">`;
                        }
                    } else if (char == "\n" || char == "\r") {
                        replaced += char;
                        displayed += `<br>`;
                    } else {
                        replaced += char;
                        replaced += char;
                    }

                    rainbowSwitch++;
                    if (rainbowSwitch == rainbowMaxColors) {
                        rainbowSwitch = 0;
                    }

                    if (i == texts.length - 1 && fonts[font].capped) {
                        replaced += `:${typeFace}-cap-end:`;
                        displayed += `<img src="letters/${typeFace}/${typeFace}-cap-end.${imageSuffix}">`;
                    }
                }

                let previewFont = document.createElement("div");
                previewFont.className = "font-example";

                let previewHeader = document.createElement("div");
                previewHeader.className = "header";

                let textSelect = document.createElement("textarea");
                textSelect.innerHTML = replaced;

                let copyText = document.createElement("button");
                copyText.innerHTML = "&nbsp;Copy&nbsp;";
                copyText.onclick = function (e) {
                    if (textSelect.value.length) {
                        textSelect.select();
                        document.execCommand("copy");
                        e.target.style.backgroundColor = "green";
                        e.target.innerHTML = "Copied";
                        setTimeout(() => {
                            textSelect.blur();
                            e.target.style = "";
                            e.target.innerHTML = "&nbsp;Copy&nbsp;";
                        }, 2000);
                    }
                }

                let previewImgs = document.createElement("div");
                previewImgs.className = "images";
                previewImgs.innerHTML += displayed;

                let fontLabel = document.createElement("label");
                fontLabel.innerHTML = fonts[font].label;


                if (fonts[font].styles && fonts[font].styles.length > 0) {
                    let selector = document.createElement("div");
                    selector.className = "select-dropdown";
                    for (let s = 0; s < fonts[font].styles.length; s++) {
                        let fStyle = fonts[font].styles[s].id;
                        let fStyleName = fonts[font].styles[s].name;
                        let selectButton = document.createElement("button");
                        selectButton.onclick = function () {
                            if (fonts[font].displayedStyle !== fStyle) {
                                fonts[font].displayedStyle = fStyle;
                                conversion();
                            }
                        };
                        if (fStyle == fonts[font].displayedStyle) {
                            selectButton.style.order = 0;
                            selectButton.className = "dropdown-arrow";
                        }
                        let iR = fonts[font].styles[s].id == "rainbow";
                        selectButton.innerHTML = `${fonts[font].label}<img src="letters/space.png">`;

                        if (fStyle == "rainbow") {
                            selectButton.innerHTML += `<img src="letters/rainbow.png">`;
                        } else {
                            for (let p = 0; p < fStyleName.length; p++) {
                                selectButton.innerHTML += `<img src="letters/${font}${fStyle}/${font}${fStyle}-${fStyleName[p]}.${imageSuffix}">`
                            }
                        }



                        selector.appendChild(selectButton);

                    }
                    previewHeader.appendChild(selector);

                } else {
                    previewHeader.appendChild(fontLabel);
                }

                previewHeader.appendChild(copyText);
                previewFont.appendChild(previewHeader);
                let overflowArea = document.createElement("div");
                overflowArea.className = "no-overflow";
                overflowArea.appendChild(textSelect);
                overflowArea.appendChild(previewImgs);

                previewFont.appendChild(overflowArea)
                examples.appendChild(previewFont);
            }
        }
        if (fontsFound == 0) {
            examples.innerHTML = `<h2>o no where'd all the fonts go</h2>`;
        }
    }

    function offsetTopFind(el, withHeight = 0) {
        let yPosition = withHeight;
        while (el.offsetParent !== null) {
            yPosition += el.offsetTop;
            el = el.offsetParent;
        }
        return yPosition;
    }

    function scrollLoadImg() {
        let winTop = window.scrollY;
        let winBot = window.innerHeight;
        let winVis = winTop + winBot;
        let unLoaded = document.querySelectorAll("[data-lz]");

        if (unLoaded.length) {
            for (let l = 0; l < unLoaded.length; l++) {
                let itemTop = offsetTopFind(unLoaded[l], 0, l == 0);
                let itemHeight = itemTop + unLoaded[l].offsetHeight;
                if (itemTop < winVis && itemHeight > winTop) {
                    let src = unLoaded[l].getAttribute("data-lz");
                    unLoaded[l].removeAttribute("data-lz");
                    unLoaded[l].src = src;
                }
            }
        }
    }
    categories.forEach((item) => {
        item.addEventListener("click", () => {
            conversion();
        });
    })
    searchinput.addEventListener("keyup", () => {
        conversion();
    });
    textinput.addEventListener("keyup", () => {
        conversion();
    });
    clearsearch.addEventListener("click", () => {
        searchinput.value = "";
        conversion();
    });

    conversion();

})();