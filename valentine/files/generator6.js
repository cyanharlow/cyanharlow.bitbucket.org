const generator = document.querySelector(".insection");
const generatedContent = document.querySelector("#generated-content");
const firstNames = ["Marg", "Paul", "Harr", "Germ", "Ezek", "Kris", "Porc", "Fran"];
const firstNames2 = ["athan", "iam", "ard", "iah", "ette", "ence"];

const lastNames = ["Cloaca", "Taint", "Fronces", "Deferens", "Tunk", "Spurt", "Fanny", "Totch", "Smeck", "Creems", "Poom","Herp", "Shromp", "Pud", "Pleen", "Hieronymus", "Spinch", "Cheam", "Tub"];

const noses = ["nose1", "nose2", "nose3"];
const mouths = ["mouth1", "mouth2"];

const eyeShapes = ["50% / 20% 20% 80% 80%", "50% / 80% 80% 20% 20%"];
const mouthShapes = ["50%", "50% / 80% 80% 20% 20%", "50% / 20% 10% 80% 80%"];
const hairShapes = ["50% / 70% 70% 25% 25%", "30% / 30% 30% 69% 69%", "30% / 70% 70% 30% 30%"];
const headShapes = ["40% 40% 50% 50% / 30% 30% 35% 35%", "50% / 30% 30% 40% 40%", "30% 30% 50% 50% / 50%", "50% / 30% 30% 70% 70%", "50% / 60% 60% 40% 40%"];

function rando(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

generator.addEventListener("click", (e) => {
  e.target.classList.add("generated");

  document.body.setAttribute("style", `
  --hairH: ${rando(0, 360)}deg;
  --hairS: ${rando(0, 100)}%;
  --hairL: ${rando(10, 80)}%;

  --skinH: ${rando(0, 360)}deg;
  --skinS: ${rando(0, 100)}%;
  --skinL: ${rando(30, 100)}%;

  --lipH: ${rando(0, 360)}deg;
  --lipS: ${rando(0, 100)}%;
  --lipL: ${rando(10, 60)}%;

  --eyeH: ${rando(0, 360)}deg;
  --eyeS: ${rando(0, 100)}%;
  --eyeL: ${rando(20, 80)}%;

  --faceWidth: ${rando(15, 30)}%;

  --faceHorizRadius: ${rando(35, 60)}%;
  --faceVertRadius: ${rando(35, 60)}%;
  --headRadius: ${headShapes[rando(0,headShapes.length - 1)]};

  --eyebrowWidth: ${rando(2, 10)}px;

  --hairRadius: ${hairShapes[rando(0,hairShapes.length - 1)]};
  --hairWidth: ${rando(20, 70)}%;
  --hairHeight: ${rando(22, 95)}%;
  --bangHeight: ${rando(5, 25)}%;

  --eyeWidth: ${rando(10, 30)}%;
  --eyeHeight: ${rando(5, 15)}%;
  --eyeShape: ${eyeShapes[rando(0, eyeShapes.length - 1)]};
  --eyeAngle: ${rando(-30, 30)}deg;

  --noseWidth: ${rando(10, 40)}%;
  --noseHeight: ${rando(15, 22)}%;
  --noseY: ${rando(38, 45)}%;
  --noseCurve: ${rando(20, 50)}%;

  --mouthRadius: ${mouthShapes[rando(0,mouthShapes.length - 1)]};
  --mouthHeight: ${rando(5, 20)}%;
  --mouthWidth: ${rando(30, 50)}%;
  --mouthY: ${rando(60, 78)}%;
  --lipTopSize: ${rando(1, 3)};
  --lipBottomSize: ${rando(1, 3)};
  --lipTopWidth: ${rando(80, 100)}%;
  --lipBottomWidth: ${rando(70, 100)}%;

  --eyeSize: ${rando(10, 50)};
  --eyeY: ${rando(25, 40)}%;
  --pupilWidth: ${rando(30, 70)}%;
  --pupilY: ${rando(10, 70)}%;
  --noseSize: ${rando(10, 50)};
  --lipSize: ${rando(10, 50)};
  --hairSize: ${rando(10, 50)};
  `);

  generatedContent.innerHTML = `

    <h5>${firstNames[rando(0, firstNames.length - 1)]}${firstNames2[rando(0, firstNames2.length - 1)]} ${lastNames[rando(0, lastNames.length - 1)]}</h5>
    <div class="hair"></div>

    <div class="neck"></div>
    <div class="shoulders"></div>
    <div class="face">


      <div class="mouth ${mouths[rando(0, mouths.length - 1)]}">
        <div class="lip top"></div>
        <div class="teeth"></div>
        <div class="lip bottom"></div>
      </div>
      <div class="philtrum"></div>
      <div class="${noses[rando(0, noses.length - 1)]}"></div>
      <div class="eyes">
        <div class="eye left"></div>
        <div class="eye right"></div>
      </div>

      <div class="bangs"></div>
    </div>
    <h6>Generate another<br>secret admirer!</h6>
  `;


})