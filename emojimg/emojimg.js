(function () {
    const interval = 30;
    const maxNum = 255 + interval;
    const textCopy = document.getElementById('texter');
    const resizeInput = document.getElementById('resizer');
    const previewArea = document.getElementById('imgPreviewArea');
    const container = document.getElementById('elementsContainer');
    const copier = document.getElementById('copytext');
    const fileup = document.getElementById("file");
    let maxWidth = 30;

    copier.addEventListener('click', (e) => {
        textCopy.select();
        document.execCommand("copy");
        copier.innerText = "Copied";
        setTimeout(() => {
            copier.innerText = "Copy Slackmoji";
        }, 1000);
    });
    resizeInput.addEventListener('change', (e) => {
        var vals = Number(e.target.value);
        if (vals <= 60 && vals >= 10) {
            maxWidth = vals;
        }
        compress();
    });

    fileup.addEventListener("change", compress, false);
    fileup.addEventListener("drop", compress, false)

    function rgbToHex(rgb) {
        var hex = Number(rgb).toString(16);
        return (hex.length < 2 ? "0" + hex : hex);
    };

    function hexString(array) {
        array.forEach((color, index) => {
            array[index] = rgbToHex(color);
        });
        return array[3] == "00" ? "spaces" : array.slice(0, 3).join("");
    };

    function roundedRGB(num) {
        return num == 255 ? num : (Math.round(num / interval) * interval);
    }

    function imageToEmoji(data) {
        var imgHTML = '';
        var slack = '';
        for (var i = 0; i < data.length; i += 4) {
            var hex = hexString([roundedRGB(data[i]), roundedRGB(data[i + 1]), roundedRGB(data[i + 2]), roundedRGB(data[i +
                3])]);
            var suffix = hex == "001e00" || hex == "1e0000" ? "h" : "";
            var lineBreak = i > 0 && ((i / 4) + 1) % maxWidth == 0;
            imgHTML += `<img src="images/${hex}${suffix}.png" >${lineBreak ? '<br>' : ''}`;
            slack += `:${hex}${suffix}:${lineBreak ? '\n' : ''}`;
        }
        textCopy.value = slack;
        container.classList.add('filled');
        previewArea.innerHTML = imgHTML;
    }


    function compress() {
        const reader = new FileReader();
        if (fileup.files && fileup.files.length) {
            reader.readAsDataURL(fileup.files[0]);
            reader.onload = event => {
                const img = new Image();
                img.src = event.target.result;
                img.onload = () => {
                        const canvasContainer = document.createElement('canvas');
                        const scaledHeight = img.height * (maxWidth / img.width);
                        canvasContainer.width = maxWidth;
                        canvasContainer.height = scaledHeight;
                        const context2D = canvasContainer.getContext('2d');
                        context2D.drawImage(img, 0, 0, maxWidth, scaledHeight);
                        var imageData = context2D.getImageData(0, 0, maxWidth, canvasContainer.height);
                        imageToEmoji(imageData.data);
                        container.style.width = (maxWidth * 20) + 'px';
                    },
                    reader.onerror = error => console.log(error);
            };
        }
        
    }

    function generateImagesForDownload() {
        var permutations = [];
        var downloads = [];
        for (var r = 0; r <= maxNum; r += interval) {
            for (var g = 0; g <= maxNum; g += interval) {
                for (var b = 0; b <= maxNum; b += interval) {
                    var c = document.createElement("canvas");
                    c.width = "128";
                    c.height = "128";
                    var context2D = c.getContext("2d");
                    context2D.beginPath();
                    context2D.rect(0, 0, 128, 128);
                    var newR = r > 255 ? 255 : r;
                    var newG = g > 255 ? 255 : g;
                    var newB = b > 255 ? 255 : b;
                    var colorVal = `rgb(${newR},${newG},${newB})`;
                    var hex = hexString([newR, newG, newB, 255]);
                    var suffix = hex == "001e00" || "1e0000" ? "h" : "";
                    context2D.fillStyle = colorVal;
                    context2D.fill();
                    var img = c.toDataURL("image/png");
                    if (permutations.indexOf(colorVal) == -1) {
                        permutations.push(colorVal);
                        downloads.push({
                            img: img,
                            download: `${hex}${suffix}.png`
                        })
                    }
                }
            }
            for (var d = 0; d < downloads.length; d++) {
                setTimeout(function (d) {
                    var anchor = document.createElement('a');
                    anchor.href = downloads[d].img;
                    anchor.download = downloads[d].download;
                    anchor.click();
                }, 200 * (d < 1 ? 1 : d), d)
            }
        }
    }

})();