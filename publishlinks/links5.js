(function () {
  var links = document.getElementById('links');
  var jumpTo = document.getElementById('copy-and-paste');

  var langSelector = document.getElementById('select-langs');
  var copiedText = document.getElementById('copytextarea');
  var copiedLinks = document.getElementById('copylinksarea');
  var copiedMagLinks = document.getElementById('copymaglinksarea');
  var fullLinks = document.getElementById('pasted');

  var openAllLinks = document.getElementById('open-all-links');

  openAllLinks.addEventListener('change', (e) => {
    let typeOfLink = e.target.value;

    let allLinks = typeOfLink == "preview-open" ? copiedLinks : copiedMagLinks;
    allLinks = allLinks.value.trim().split('\n');

    for (var d = 0; d < allLinks.length; d++) {
      setTimeout(function (d) {
          var anchor = document.createElement('a');
          anchor.href = allLinks[d];
          anchor.setAttribute('target', "_blank")
          anchor.click();
      }, 100 * (d < 1 ? 1 : d), d)
    }

    openAllLinks.value = "null";
  });

  var radioSets = ['env', 'tab', 'task'];

  var properties = {
    env: "proof.marketing.internal.",
    tab: "same",
    task: "publishing",
    lang: "en"
  };

  function convert() {
    var bucket = [];
    var formatted = links.value.trim().split('\n');
    var lang = properties.lang == 'en' ? '' : '/' + properties.lang;

    fullLinks.innerHTML = copiedText.innerHTML = copiedLinks.innerHTML = copiedMagLinks.innerHTML = "";
    formatted.forEach(link => {
      link = link.trim();
      if (properties.task == "importing") {
        link = link.replace(/^(?!Skip).+/g, '');
      }
      link = link.replace(/http(.*?):\/\/(.*?)\//, '/').replace(/(.*?)(?<!\d)\//, '/').replace(/\.$/, '').replace(/^([^/]*?)$/, '').replace(/\s(.*?)$/, '');
      // link = link.replace(/http(.*?):\/\/(.*?)\//, '/').replace(/(.*?)\//, '/').replace(/\.$/, '').replace(/^([^/]*?)$/, '').replace(/\s(.*?)$/, '');

      let previewLink = link;
      let domain = `atlassian.com`;
      let magnoliaLink = `/.magnolia/admincentral#app:pages:browser;${link}:searchview:${link}`;

      if (properties.env == "truth.marketing.internal." && link.indexOf('/bitbucket/product') == 0) {
        previewLink = previewLink.replace('/bitbucket/product', '/bitbucket')
      }

      if (properties.env == "") {
        link = link.replace('/wac', '');
        if (link.indexOf('/bitbucket') == 0) {
          link = link.replace('/bitbucket', '');
          domain = `bitbucket.org`;
        }
        previewLink = link;
        magnoliaLink = link;
      }


      if (link.trim().length && bucket.indexOf(link) < 0) {
        bucket.push(link);
        fullLinks.innerHTML += `<a href="https://${properties.env}${domain}${magnoliaLink}" target="${properties.tab == "same" ? 'linkwindow' : '_blank'}">${link}</a>`;
        copiedText.innerHTML += `${lang}${link}
`;
        copiedLinks.innerHTML += `https://${properties.env}${domain}${lang}${previewLink}
`;
        copiedMagLinks.innerHTML += `https://${properties.env}${domain}${magnoliaLink}
`;
      }
    });
    document.getElementById('reappear').classList.remove('invis');
    window.scrollTo(0, jumpTo.offsetTop)
  }

  radioSets.forEach(buttonName => {
    var buttons = document.getElementsByName(buttonName);
    Array.from(buttons).forEach(button => {
      button.addEventListener('change', (e) => {
        properties[buttonName] = e.target.value;
        convert();
      });
    });
  });

  langSelector.addEventListener('change', (e) => {
    properties.lang = e.target.value;
    convert();
  });

  ['paste', 'change', 'keyup'].forEach(evt => {
    links.addEventListener(evt, (e) => {
      convert()
    });
  });

  document.getElementById('copies-area').addEventListener('click', function (e) {
    if (e.target.tagName == "BUTTON") {
      let toCopy = document.getElementById(e.target.id + 'area');
      toCopy.select();
      document.execCommand("copy");
      e.target.className += " copy-success";
      setTimeout(function () {
        e.target.className = "";
      }, 1000);
    }

  })

})();